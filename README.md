#Android Third Party Library Encryption App

*Version 1.0*

The following project encrypts Device data using the Android Keystore and sends the encrypted information to an external API.

The encryption and web API methods are implemented with an external Android Library, which is then imbedded into the Sample App.

Intrumental Tests have been implemented to Unit Test Library methods and App UI.