package com.example.devicedatagatherer.common

import android.util.Base64
import java.security.Key
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec

class CipherWrapper {

    companion object {
        var TRANSFORMATION_SYMMETRIC = "AES/CBC/PKCS7Padding"

        val IV_SEPARATOR = "]"
    }

    val cipher: Cipher = Cipher.getInstance(TRANSFORMATION_SYMMETRIC)

    fun encrypt(data: String, key: Key?): String {
        cipher.init(Cipher.ENCRYPT_MODE, key)

        var result = ""
        val iv = cipher.iv
        val ivString = Base64.encodeToString(iv, Base64.DEFAULT)
        result = ivString + IV_SEPARATOR

        val bytes = cipher.doFinal(data.toByteArray())
        result += Base64.encodeToString(bytes, Base64.DEFAULT)

        return result
    }
}