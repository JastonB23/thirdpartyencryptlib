package com.example.devicedatagatherer.service.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CustomerEntity(@Expose @SerializedName("customerId") var customerId: String? = null,
                          @Expose @SerializedName("email") var email: String? = null,
                          @Expose @SerializedName("name") var name: String? = null)