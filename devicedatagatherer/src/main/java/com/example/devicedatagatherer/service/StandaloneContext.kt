package com.example.devicedatagatherer.service

import android.content.Context

object StandaloneContext {

    private var context: Context? = null

    /**
     * Get Context instance
     */
    fun getContext(): Context = context ?: error("StandAloneContext context instance is null")

    /**
     * Setup new Context instance
     */
    fun setup(instance: Context) {
        context = instance
    }
}