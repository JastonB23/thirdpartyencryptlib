package com.example.devicedatagatherer.service

import com.example.devicedatagatherer.service.entities.DeviceDataEntity
import com.example.devicedatagatherer.service.network.Api

class APIService {

    suspend fun sendDeviceData(data: DeviceDataEntity): Boolean {
        try {
            val response = Api.invoke().sendData(data).await()
            return response.isSuccessful
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }
}