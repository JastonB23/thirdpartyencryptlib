package com.example.devicedatagatherer.service.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DeviceEntity (@Expose @SerializedName("deviceId") var deviceID: String? = null,
                         @Expose @SerializedName("custom") var deviceData: String? = null)