package com.example.devicedatagatherer.service.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DeviceDataEntity(@Expose @SerializedName("timestamp") val timestamp: Long,
                            @Expose @SerializedName("customer") var customer: CustomerEntity? = null,
                            @Expose @SerializedName("device") var device: DeviceEntity? = null)