package com.example.devicedatagatherer.service

import android.content.Context
import com.example.devicedatagatherer.common.*

class EncryptionService {

    companion object {
        val DEFAULT_KEY_STORE_NAME = "default_keystore"
        val MASTER_KEY = "MASTER_KEY"
    }

    private val context = StandaloneContext.getContext()
    private val keyStoreWrapper = KeystoreWrapper(context, DEFAULT_KEY_STORE_NAME)
    private val cipher = CipherWrapper()

    init {
        createMasterKey()
    }

    /**
     * Encrypt user password and Secrets with created master key.
     */
    fun encrypt(data: String): String {
        val masterKey = keyStoreWrapper.getDefaultKeyStoreSymmetricKey(MASTER_KEY)
        val encryptedSdata = cipher.encrypt(data, masterKey)
        return hashSHA1(encryptedSdata)
    }

    /**
     * Create and save cryptography key, to protect Secrets with.
     */
    private fun createMasterKey() {
        keyStoreWrapper.createDefaultKeyStoreSymmetricKey(MASTER_KEY)
    }
}