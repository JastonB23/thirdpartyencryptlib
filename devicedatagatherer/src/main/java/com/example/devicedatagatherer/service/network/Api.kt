package com.example.devicedatagatherer.service.network

import com.example.devicedatagatherer.BuildConfig
import com.example.devicedatagatherer.service.entities.DeviceDataEntity
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import okhttp3.Interceptor
import okhttp3.OkHttpClient

const val BASE_URL: String = "https://api-staging.ravelin.com/"

interface Api {
    companion object {
        // This is a static method for API
        operator fun invoke(): Api {

            // Modifies URL before it's fired
            val requestInterceptor = Interceptor { chain ->
                val url = chain.request().url()

                val request = chain.request()
                    .newBuilder().url(url)
                    .addHeader("Authorization", "Bearer " + BuildConfig.authtoken)
                    .build()

                return@Interceptor chain.proceed(request)
            }

            // Deal with client requests, adds the interceptor
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .build()

            // May use DI to deal with this later on...
            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Api::class.java)
        }
    }

    @Headers("Content-Type: application/json")
    @POST("v2/customer")
    fun sendData(@Body body: DeviceDataEntity): Deferred<Response<DeviceDataEntity>>
}