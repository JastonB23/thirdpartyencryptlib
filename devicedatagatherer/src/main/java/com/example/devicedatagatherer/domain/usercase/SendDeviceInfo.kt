package com.example.devicedatagatherer.domain.usercase

import android.util.Log
import com.example.devicedatagatherer.domain.model.DataContainer
import com.example.devicedatagatherer.service.APIService
import com.example.devicedatagatherer.service.entities.CustomerEntity
import com.example.devicedatagatherer.service.entities.DeviceDataEntity
import com.example.devicedatagatherer.service.entities.DeviceEntity
import com.example.devicedatagatherer.service.network.Api
import kotlinx.coroutines.runBlocking
import java.util.*

val api = APIService()

fun sendDeviceInfo(container: DataContainer?): Boolean {
    var successfullySent: Boolean = false

    val deviceData = createDeviceDataEntity(container)

    runBlocking {
        successfullySent = api.sendDeviceData(deviceData)
    }

    return successfullySent
}

fun createDeviceDataEntity(container: DataContainer?): DeviceDataEntity {
    val customerId = container?.customerId
    val data = container?.data

    val customEntity = CustomerEntity(customerId, customerId, customerId)
    val deviceEntity = DeviceEntity("123456", data)

    val timestamp = Date().time

    return DeviceDataEntity(timestamp, customEntity, deviceEntity)
}