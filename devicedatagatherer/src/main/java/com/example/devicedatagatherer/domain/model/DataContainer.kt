package com.example.devicedatagatherer.domain.model

data class DataContainer(val customerId: String, val data: String?)