package com.example.devicedatagatherer.domain.model

data class Customer(val customerId: String, val email: String, val name: String)