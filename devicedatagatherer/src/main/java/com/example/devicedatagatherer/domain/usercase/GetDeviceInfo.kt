package com.example.devicedatagatherer.domain.usercase

import android.util.Log
import android.os.Build
import com.example.devicedatagatherer.domain.model.*
import kotlinx.coroutines.runBlocking
import com.example.devicedatagatherer.common.*
import com.example.devicedatagatherer.service.EncryptionService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

val encryption = EncryptionService()

fun getDeviceInfo(email: String): String? {
    var text: String? = null
    runBlocking {
        text = executeEncryption(email)
    }
    return text
}

private suspend fun executeEncryption(s: String): String {
    val json = accessDeviceData(s)
    Log.e("Device Data JSON = ", json)

    val encryptedJson: String = encryption.encrypt(json)
    Log.e("Encrypted JSON = ", encryptedJson)

    return encryptedJson
}

private fun accessDeviceData(email: String): String {

    val device = Device(Build.ID, Build.MODEL, System.getProperty("os.name"), Build.BRAND, Build.MANUFACTURER)

    val customer = Customer(email, email, email)

    val deviceData = DeviceData(System.currentTimeMillis(), customer, device)

    return convertDataToJson(deviceData)
}