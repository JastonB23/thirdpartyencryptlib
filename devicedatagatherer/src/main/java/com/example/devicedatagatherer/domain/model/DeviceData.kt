package com.example.devicedatagatherer.domain.model

data class DeviceData (val timestamp: Long, val customer: Customer, val device: Device)