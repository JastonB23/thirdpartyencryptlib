package com.example.devicedatagatherer.domain.model

data class Device (val deviceId: String, val model: String, val os: String?, val type: String, val manufacturer: String)