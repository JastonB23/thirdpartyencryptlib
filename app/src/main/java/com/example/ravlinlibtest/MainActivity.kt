package com.example.ravlinlibtest

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.example.devicedatagatherer.domain.model.DataContainer
import com.example.devicedatagatherer.domain.usercase.*
import com.example.devicedatagatherer.service.StandaloneContext
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var container: DataContainer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Set on Click listeners
        btn_gatherData.setOnClickListener {
            hideKeyboard()

            val email = edit_emailAddress.text.toString()
            val encryptedData = getDeviceInfo(email)

            if (!encryptedData.isNullOrEmpty()) {
                container = DataContainer(email, encryptedData)
                btn_sendData.isEnabled = true
            } else displayEncrytionError()
        }

        var sentSuccessfully: Boolean = false

        btn_sendData.setOnClickListener {
            sentSuccessfully = sendDeviceInfo(container)
            displayResponseResult(sentSuccessfully)
            btn_sendData.isEnabled = false
        }

    }

    fun displayEncrytionError() {
        val msg = "Encryption Failure"
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    fun displayResponseResult(isSuccess: Boolean) {
        val msg = if (isSuccess) "Data Submission Successful!" else "Data Submission Failure"
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    fun hideKeyboard() {
        val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(currentFocus.windowToken, InputMethodManager.SHOW_FORCED)
    }
}
