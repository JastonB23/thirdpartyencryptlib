package com.example.ravlinlibtest

import android.app.Application
import com.example.devicedatagatherer.service.StandaloneContext

class RavlinApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        StandaloneContext.setup(this)
    }
}