package com.example.ravlinlibtest

import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule

/**
 * Tests for the Main Activity screen, the main screen which contains the data gathering and sending buttons
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {

    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    fun isGatherDeviceButtonPresent() {
        Espresso.onView(withId(R.id.btn_gatherData)).check(matches(isDisplayed()))
    }

    @Test
    fun isSendDeviceDataButtonPresent() {
        Espresso.onView(withId(R.id.btn_sendData)).check(matches(isDisplayed()))
    }

    @Test
    fun clickGatherDataButton_gathersDeviceData() {

    }
}